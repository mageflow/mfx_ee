<?php
/**
 * Created by PhpStorm.
 * User: urmas
 * Date: 12/4/14
 * Time: 5:15 PM
 */

class Mageflow_Connectee_Model_Handler_Promotion_Rule_CatalogTest  extends PHPUnit_Framework_TestCase
{
    /**
     * @var Mageflow_Connectee_Model_Handler_Promotion_Rule_Catalog
     */
    protected $object;
    protected $testmodel;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Mageflow_Connectee_Model_Handler_Promotion_Rule_Catalog();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers Mageflow_Connectee_Model_Handler_Promotion_Rule_Catalog::packData
     *
     * public function packData(Mage_Core_Model_Abstract $model)
     */
    public function testPackData()
    {
        $this->assertTrue(method_exists($this->object, 'packData'));
    }

    /**
     * @covers Mageflow_Connectee_Model_Handler_Promotion_Rule_Catalog::getPreview
     *
     */
    public function testGetPreview()
    {
        $this->assertTrue(method_exists($this->object, 'getPreview'));
    }

    /**
     * @covers Mageflow_Connectee_Model_Handler_Promotion_Rule_Catalog::processData
     *
     * public function processData(array $data = array())
     */
    public function testProcessData()
    {
        $this->assertTrue(method_exists($this->object, 'processData'));
    }
}