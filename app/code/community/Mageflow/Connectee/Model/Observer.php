<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Observer.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Observer
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Observer extends Mageflow_Connect_Model_Observer
{

	/**
	 * @return Mageflow_Connect_Helper_Type
	 */
	public function getTypeHelper() {
		return Mage::helper( 'mageflow_connectee/type' );
	}
    
    
    /**
     * Saves changeset item for allowed types
     *
     * @param Varien_Event_Observer $observer
     */
    public function onSaveChangesetItem(Varien_Event_Observer $observer)
    {
        try {
            $e = $observer->getEvent();
            if ($e instanceof Varien_Event) {

                $o = $e->getData('object');

                $type = !is_null($o) ? get_class($o) : null;

                $this->log('Type: ' . $type);

                if (null !== $o && null !== $type
                    && (
                        $this->getTypeHelper()->isTypeEnabled($type)
                        || $o instanceof Mage_Core_Model_Config_Data
                    )
                    && !($o instanceof Mageflow_Connect_Model_Changeset_Item)
                ) {

                    $this->log($this->getTypeHelper()->getHandlerClass($type, $o));

                    /**
                     * @var stdClass $type
                     */
                    $type = $this->getTypeHelper()->getType($type, $o);

                    /**
                     * @var Mageflow_Connect_Helper_Data $dataHelper
                     */
                    $dataHelper = Mage::helper('mageflow_connectee/data');

                    $csItemModel = $dataHelper->createChangesetFromItem($type, $o);

                    /**
                     * Save newly created changeset item MFGUID to registry
                     * so that other parts of the process can access it
                     */
                    if (!$o->isObjectNew()) {
                        Mage::register($o->getMfGuid(), $csItemModel->getMfGuid(), true);
                    }
                }
            }
        } catch (Exception $ex) {
            $this->log($ex->getMessage());
            $this->log($ex->getTraceAsString());
        }
    }
}