<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Page.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Handler_Cms_Page
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Handler_Cms_Page
    extends Mageflow_Connect_Model_Handler_Cms_Page
{
    /**
     * update or create cms/page from data array
     *
     * @param array $data
     *
     * @return array
     * @throws Exception
     */
    public function processData(array $data)
    {
        $data = isset($data[0]) ? $data[0] : $data;
        $message = null;

        try {

            if (isset($data['stores']) && is_array($data['stores']) && count($data['stores'])) {
                $storeIdList = $this->getStoreIdListByCodes($data['stores']);
                if ($storeIdList == array()) {
                    throw new Exception('no matching stores');
                }
                if (count($data['stores']) != count($storeIdList)) {
                    $message =
                        "Notice: following store views are missing from target: "
                        . $this->getMissingStores($data['stores']);
                }
                $data['stores'] = $storeIdList;
            } else {
                $data['stores'] = array();
            }

            /**
             * @var Mage_Cms_Model_Page $model
             */
            $model = $this->findModel('cms/page', $data['mf_guid'],
                array('field'  => 'identifier', 'value' => $data['identifier'],
                      'stores' => $data['stores'])
            );
            $data['page_id'] = $model->getPageId();

            $isUnderVersionControl = $data['under_version_control'];
            $data['under_version_control'] = 0;

            $userEntity = null;
            if (isset($data['user_id'])) {
                $userEntity = Mage::getModel('admin/user')
                    ->load($data['user_id'], 'mf_guid');
            }

            if (is_null($userEntity) || ($userEntity->getUserId() < 1)){
                $userEntity = Mage::getModel('admin/user')
                    ->getCollection()->getFirstItem();
            }

            if (!Mage::getSingleton('admin/session')->isLoggedIn()) {
                Mage::getSingleton('admin/session')->setUser($userEntity);
            }

            $publishedRevisionId = $data['published_revision_id'];
            $data['disable_creating_changeset'] = true;
            $model->setData($data);
            $model->save();
            $model->setWebsiteRoot($data['website_root']);
            $model->setIsActive($data['is_active']);
            $model->setUnderVersionControl($isUnderVersionControl);
            $model->setDisableCreatingChangeset(false);
            $model->save();

            $pageId = $model->getPageId();

            $versionIdList = array();
            $revisionIdList = array();

            foreach ($data['versions'] as $versionData) {

                if (count($versionData['revisions']) < 1) {
                    continue;
                }

                $versionData['page_id'] = $pageId;
                unset($versionData['version_id']);

                $revisionData = $versionData['revisions'][0];
                $revisionData['page_id'] = $pageId;
                unset($revisionData['revision_id']);
                if (isset($revisionData['user_id'])) {
                    $revisionData['user_id'] = Mage::getModel('admin/user')
                        ->load($revisionData['user_id'], 'mf_guid')->getId();
                    if (is_null($revisionData['user_id'])) {
                        $revisionData['user_id'] = $userEntity->getId();
                    }
                } else {
                    $revisionData['user_id'] = null;
                }
                unset($versionData['revisions'][0]);

                $versionData['initial_revision_data'] = $revisionData;

                $versionEntity = Mage::getModel('enterprise_cms/page_version');
                $versionEntity->setData($versionData);
                $versionEntity->save();
                $versionIdList[] = $versionEntity->getVersionId();

                $currentRevisionEntity = Mage::getModel('enterprise_cms/page_revision')
                    ->load($versionEntity->getVersionId(), 'version_id');
                $revisionIdList[] = $currentRevisionEntity->getRevisionId();

                foreach ($versionData['revisions'] as $revisionData) {
                    $oldRevisionId = $revisionData['revision_id'];
                    unset($revisionData['revision_id']);
                    $revisionData['version_id'] = $versionEntity->getVersionId();
                    $revisionData['page_id'] = $pageId;

                    $revisionData['user_id'] = Mage::getModel('admin/user')
                        ->load($revisionData['user_id'], 'mf_guid')->getId();
                    if (is_null($revisionData['user_id'])) {
                        $revisionData['user_id'] = $userEntity->getId();
                    }

                    $revisionEntity = Mage::getModel('enterprise_cms/page_revision');
                    $revisionEntity->setData($revisionData);
                    $revisionEntity->save();
                    $revisionIdList[] = $revisionEntity->getRevisionId();
                    if ($oldRevisionId == $publishedRevisionId) {
                        $revisionEntity->publish();
                        $revisionEntity->save();
                    }
                }
            }

            $nodeHandler = Mage::getModel('Mageflow_Connectee_Model_Handler_Cms_Node');
            foreach ($data['nodes'] as $nodeData) {
                $nodeData['page_id'] = $model->getMfGuid();
                $nodeData['disable_creating_changeset'] = true;
                $nodeHandler->processData($nodeData);
            }

            $oldRevisionCollection = Mage::getModel('enterprise_cms/page_revision')
                ->getCollection()
                ->addFieldToFilter('page_id', $pageId)
                ->addFieldToFilter('revision_id', array('nin' => $revisionIdList));

            foreach ($oldRevisionCollection as $revision) {
                $revision->delete();
            }

            $oldVersionCollection = Mage::getModel('enterprise_cms/page_version')
                ->getCollection()
                ->addFieldToFilter('page_id', $pageId)
                ->addFieldToFilter('version_id', array('nin' => $versionIdList));

            foreach ($oldVersionCollection as $version) {
                $version->delete();
            }

        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $this->log($ex->getMessage());
            $this->log($ex->getTraceAsString());
        }

        return $this->sendProcessingResponse($model, $message);
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     *
     * @return stdClass
     */
    public function packData(Mage_Core_Model_Abstract $model)
    {
        $c = parent::packData($model);

        //$c->raw_data = $model->getData();

        $versionCollection = Mage::getModel('enterprise_cms/page_version')
            ->getCollection()
            ->addFieldToFilter('page_id', $model->getPageId());

        $nodeCollection = Mage::getModel('enterprise_cms/hierarchy_node')
            ->getCollection()
            ->addFieldToFilter('page_id', $model->getPageId());

        foreach ($versionCollection as $versionEntity) {
            $versionData = $versionEntity->getData();
            $revisionCollection = Mage::getModel('enterprise_cms/page_revision')
                ->getCollection()
                ->addFieldToFilter('page_id', $model->getPageId())
                ->addFieldToFilter('version_id', $versionEntity->getVersionId());

            $versionData['revisions'] = array();
            foreach ($revisionCollection as $revisionEntity) {
                $revisionData = $revisionEntity->getData();
                $revisionData['user_id'] = Mage::getModel('admin/user')
                    ->load($revisionData['user_id'])->getMfGuid();
                $c->user_id = $revisionData['user_id'];
                $versionData['revisions'][] = $revisionData;
            }

            $c->versions[$versionEntity->getVersionId()] = $versionData;
        }

        $nodeHandler = Mage::getModel('Mageflow_Connectee_Model_Handler_Cms_Node');
        foreach ($nodeCollection as $nodeEntity) {
            $nodeEntity->setData('pack_for_page', true);
            $nodeData = $nodeHandler->packData($nodeEntity);
            if (isset($nodeData->xpath[0]) && $nodeData->xpath[0] == $nodeData->mf_guid) {
                $nodeData->xpath = array();
            }
            $c->nodes[] = $nodeData;
        }

        return $c;
    }

    /**
     * Calculates model's checksum over its significant fields
     *
     * @param Mage_Core_Model_Abstract $model
     *
     * @return string
     */
    public function calculateChecksum(Mage_Core_Model_Abstract $model)
    {
        $packedData = $this->packData($model);
        unset($packedData->updated_at);
        unset($packedData->created_at);
        unset($packedData->mf_guid);
        unset($packedData->deploymentpackage);
        $dataStr = json_encode($packedData);

        return sha1($dataStr);
    }
}
