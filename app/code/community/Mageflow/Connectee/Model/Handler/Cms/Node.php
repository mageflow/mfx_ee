<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Node.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Handler_Cms_Node
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Handler_Cms_Node
    extends Mageflow_Connectee_Model_Handler_Cms_Abstract
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function processData(array $data)
    {

        $data = isset($data[0]) ? $data[0] : $data;

        $message = null;

        try {
            $model = $this->findModel('enterprise_cms/hierarchy_node', $data['mf_guid']);

            switch ($data['scope']) {
                case 'default':
                case 'website':
                    $website = Mage::getModel('core/website')
                        ->load($data['scope_id'], 'mf_guid');
                    $data['scope_id'] = $website->getWebsiteId();
                    $this->createTopNode($data['scope'], $data['scope_id']);
                    break;
                case 'store':
                    $store = Mage::getModel('core/store')
                        ->load($data['scope_id'], 'mf_guid');
                    $data['scope_id'] = $store->getStoreId();
                    $this->createTopNode($data['scope'], $data['scope_id']);
                    break;
            }

            if (isset($data['xpath'])) {
                $xPath = '';
                foreach($data['xpath'] as $key => $pathNodeMfGuid) {
                    $xPath .= Mage::getModel('enterprise_cms/hierarchy_node')
                        ->load($pathNodeMfGuid, 'mf_guid')->getNodeId();
                    $xPath .= '/';
                }
                $data['xpath'] = $xPath;
            }

            if ($model->getNodeId() > 0) {
                $data['node_id'] = $model->getNodeId();
            }

            if (isset($data['meta_data'])) {
                $metaData = $data['meta_data'];
                unset($data['meta_data']);
                $data = array_merge($data, $metaData);
            }

            if (isset($data['page_id'])) {
                $data['page_id'] = Mage::getModel('cms/page')
                    ->load($data['page_id'], 'mf_guid')->getPageId();
            }

            if (isset($data['parent_node_id'])) {
                $data['parent_node_id'] = Mage::getModel('enterprise_cms/hierarchy_node')
                    ->load($data['parent_node_id'], 'mf_guid')->getNodeId();
            }

            $model->setData($data);
            $model->save();

        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->log($e->getMessage());
            $this->log($e->getTraceAsString());
        }
        return $this->sendProcessingResponse($model, $message);
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     *
     * @return null|stdClass
     */
    public function packData(Mage_Core_Model_Abstract $model)
    {
        $c = $this->packModel($model);

        if ($c->scope == 'default' || $c->scope == 'website') {
            $c->scope_id = Mage::getModel('core/website')->load($c->scope_id)
                ->getMfGuid();
        } else {
            $c->scope_id = Mage::getModel('core/store')->load($c->scope_id)
                ->getMfGuid();
        }

        $c->xpath = explode('/', $c->xpath);
        foreach ($c->xpath as $key => $nodeId) {
            $c->xpath[$key] = Mage::getModel('enterprise_cms/hierarchy_node')
                ->load($nodeId)->getMfGuid();
            if ($nodeId == $model->getNodeId()) {
                unset($c->xpath[$key]);
            }
        }

        if (!empty($c->parent_node_id)) {
            $c->parent_node_id = Mage::getModel('enterprise_cms/hierarchy_node')
                ->load($c->parent_node_id)->getMfGuid();
        }

        if (!empty($c->page_id)) {
            if ($model->getData('pack_for_page')) {
                $c->page_id = Mage::getModel('cms/page')
                    ->load($c->page_id)->getMfGuid();
            } else {
                $c = null;
            }
        }

        $mfGuid = $model->getMfGuid();
        $mfGuidNeedsUpdateFlag = $model->getData('mf_guid_needs_update');

        $model->load($model->getNodeId());

        if ($mfGuidNeedsUpdateFlag && $mfGuid) {
            $model->setMfGuid($mfGuid);
            $model->setData('disable_creating_changeset');
            $model->save();
        }

        if (!property_exists($c, 'mf_guid')) {
            $c->mf_guid = $model->getMfGuid();
        }

        $c->meta_data = $model->getResource()->getTreeMetaData($model);
        unset($c->meta_data['node_id']);

        if (!property_exists($c, 'identifier') && !property_exists($c, 'page_id')) {
            $c = null;
        }
        return $c;
    }

    /**
     * if zero level node does not exist for the scope, create it
     *
     * @param $scope
     * @param $scopeId
     *
     * @return mixed
     */
    private function createTopNode($scope, $scopeId)
    {
        $topNode = Mage::getModel('enterprise_cms/hierarchy_node')
            ->getCollection()
            ->addFieldToFilter('identifier', array('null' => true))
            ->addFieldToFilter('level', '0')
            ->addFieldToFilter('scope', $scope)
            ->addFieldToFilter('scope_id', $scopeId)
            ->getFirstItem();

        if ($topNode->getNodeId() < 1) {
            $topNode->setData(array(
                    'parent_node_id' => null,
                    'page_id' => null,
                    'sort_order' => '0',
                    'xpath' => null,
                    'identifier'=> null,
                    'label'=> null,
                    'level'=> '0',
                    'request_url'=> null,
                    'scope'=> $scope,
                    'scope_id'=> $scopeId
                ));
            $topNode->save();
        }

        return $topNode;
    }

    /**
     * @param Mageflow_Connect_Model_Interfaces_Changeitem $row
     * @return string|void
     */
    public function getPreview(Mageflow_Connect_Model_Interfaces_Changeitem $row)
    {
        $content = json_decode($row->getContent());
        if ($content->label) {
            $output = sprintf('%s (url: %s)', $content->label, $content->identifier);
        }else{
            $output = $content->identifier;
        }
        return $output;
    }

    /**
     * loads model by id and returns it's mf_guid
     * there are occasions when we need to ask it more sternly
     *
     * @param Enterprise_Cms_Model_Hierarchy_Node $model
     *
     * @return mixed
     */
    public function returnMfGuid(Enterprise_Cms_Model_Hierarchy_Node $model)
    {
        $nodeId = $model->getNodeId();
        $model = Mage::getModel('enterprise_cms/hierarchy_node')
            ->getCollection()
            ->addFilter('node_id', $nodeId)
            ->getFirstItem();

        $mfGuid = $model->getMfGuid();

        return $mfGuid;
    }
}
