<?php
/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Banner.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Handler_Cms_Banner
 *
 * @category   MFX
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Handler_Cms_Banner
    extends Mageflow_Connectee_Model_Handler_Cms_Abstract
{
    /**
     * update or create cms/page from data array
     *
     * @param array $data
     *
     * @return array
     * @throws Exception
     */
    public function processData(array $data)
    {
        $data = isset($data[0]) ? $data[0] : $data;
        $message = null;

        $storeIdList = $this->getStoreIdArray();
        $storeIdList[0] = 0;

        try {
            $model = Mage::getModel('enterprise_banner/banner')
                ->load($data['mf_guid'], 'mf_guid');

            if ($model->getBannerId() > 0) {
                $data['banner_id'] = $model->getBannerId();
            }

            $model->setName($data['name']);
            $model->setMfGuid($data['mf_guid']);
            $model->setCreatedAt($data['created_at']);
            $model->setUpdatedAt($data['updated_at']);
            $model->setTypes($data['types']);
            $model->setIsEnabled($data['is_enabled']);
            $model->setUseCustomerSegment($data['use_customer_segment']);

            if (isset($data['store_contents'])) {
                foreach ($data['store_contents'] as $storeCode => $storeContent) {
                    if (!is_null($storeContent)) {
                        $data['store_contents'][$storeIdList[$storeCode]] = $storeContent;
                    }
                    if ($storeCode != $storeIdList[$storeCode]) {
                        unset($data['store_contents'][$storeCode]);
                    }
                }
            }
            $model->setStoreContents($data['store_contents']);

            if (isset($data['store_contents_not_use'])) {
                $notUse = array();
                foreach ($data['store_contents_not_use'] as $storeCode) {
                    $notUse[] = $storeIdList[$storeCode];
                }
                $model->setStoreContentsNotUse($notUse);
            }

            if (isset($data['catalogrules'])) {
                $ruleIds = array();
                foreach ($data['catalogrules'] as $ruleMfGuid) {
                    $ruleEntity = Mage::getModel('catalogrule/rule')
                        ->load($ruleMfGuid, 'mf_guid');
                    if ($ruleEntity->getRuleId()) {
                        $ruleIds[] = $ruleEntity->getRuleId();
                    }
                }
                if (count($ruleIds)) {
                    $model->setBannerCatalogRules($ruleIds);
                }
            }

            if (isset($data['salesrules'])) {
                $ruleIds = array();
                foreach ($data['catalogrules'] as $ruleMfGuid) {
                    $ruleEntity = Mage::getModel('salesrule/rule')
                        ->load($ruleMfGuid, 'mf_guid');
                    if ($ruleEntity->getRuleId()) {
                        $ruleIds[] = $ruleEntity->getRuleId();
                    }
                }
                if (count($ruleIds)) {
                    $model->setBannerSalesRules($ruleIds);
                }
            }

            if (isset($data['customer_segment_ids'])) {
                $segments = array();
                foreach ($data['customer_segment_ids'] as $customerSegmentMfGuid) {
                    $customerSegmentEntity =
                        Mage::getModel('enterprise_customersegment/segment')
                            ->load($customerSegmentMfGuid, 'mf_guid');
                    $segments[] = $customerSegmentEntity->getSegmentId();
                }
                $model->setCustomerSegmentIds($segments);
            }

            $model->save();

        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $this->log($ex->getMessage());
            $this->log($ex->getTraceAsString());
        }

        return $this->sendProcessingResponse($model, $message);
    }

    /**
     * pack data for changeset
     *
     * @param Mage_Core_Model_Abstract $model
     *
     * @return stdClass
     */
    public function packData(Mage_Core_Model_Abstract $model)
    {
        $c = new stdClass();

        $bannerCatalogRules = Mage::getModel('enterprise_banner/catalogrule')
            ->getCollection()
            ->addFieldToFilter('banner_id', $model->getBannerId());
        foreach ($bannerCatalogRules as $bannerCatalogRule) {
            $catalogRule = Mage::getModel('catalogrule/rule')
                ->load($bannerCatalogRule->getRuleId());
            $c->catalogrules[] = $catalogRule->getMfGuid();
        }
        $bannerSalesRules = Mage::getModel('enterprise_banner/salesrule')
            ->getCollection()
            ->addFieldToFilter('banner_id', $model->getBannerId());
        foreach ($bannerSalesRules as $bannerSalesRule) {
            $salesRule = Mage::getModel('salesrule/rule')
                ->load($bannerSalesRule->getRuleId());
            $c->salesrules[] = $salesRule->getMfGuid();
        }

        $resourceModel = Mage::getModel('Mageflow_Connectee_Model_Handler_Cms_Banner_Resource');
        $c->customer_segment_ids = array();
        $c->use_customer_segment = 0;

        foreach ($resourceModel->getSegmentData($model->getBannerId()) as $segmentId) {
            $customerSegment =
                Mage::getModel('enterprise_customersegment/segment')
                    ->load($segmentId);
            $c->customer_segment_ids[] = $customerSegment->getMfGuid();
            $c->use_customer_segment = 1;
        }

        $storeCodeList = $this->getStoreCodeArray();
        $storeCodeList[0] = 0;
        $c->store_contents = array();
        $c->store_contents_not_use = $this->getStoreIdArray();
        $storeContents = $model->getStoreContents();

        foreach ($storeCodeList as $storeId => $storeCode) {
            $c->store_contents[$storeCode] = $storeContents[$storeId];
            unset($c->store_contents_not_use[$storeCode]);
        }

        $c->name = $model->getName();
        $c->is_enabled = $model->getIsEnabled();
        $c->types = $model->getTypes();
        $c->created_at = $model->getCreatedAt();
        $c->updated_at = $model->getUpdatedAt();
        $c->mf_guid = $model->getMfGuid();
        return $c;
    }
}