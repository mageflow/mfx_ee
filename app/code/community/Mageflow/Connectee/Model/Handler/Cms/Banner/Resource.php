<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Resource.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Handler_Cms_Banner_Resource
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Handler_Cms_Banner_Resource
    extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Sales rule table name
     *
     * @var string
     */
    protected $_salesRuleTable;

    /**
     * Catalog rule table name
     *
     * @var string
     */
    protected $_catalogRuleTable;

    /**
     * Contents table name
     *
     * @var string
     */
    protected $_contentsTable;

    /**
     * Customer segment relation table name
     *
     * @var string
     */
    protected $_customerSegmentTable;

    /**
     * Initialize banner resource model
     *
     */
    protected function _construct()
    {
        $this->_init('enterprise_banner/banner', 'banner_id');
        $this->_salesRuleTable = $this->getTable('enterprise_banner/salesrule');
        $this->_catalogRuleTable = $this->getTable(
            'enterprise_banner/catalogrule'
        );
        $this->_contentsTable = $this->getTable('enterprise_banner/content');
        $this->_customerSegmentTable = $this->getTable(
            'enterprise_banner/customersegment'
        );
    }

    /**
     * retrives customer segment data
     *
     * @param $bannerId
     *
     * @return array
     */
    public function getSegmentData($bannerId)
    {
        $read = $this->_getReadAdapter();
        $select = $read->select()
            ->from($this->_customerSegmentTable)
            ->where('banner_id = ?', $bannerId);

        $segmentsArray = array();

        if ($data = $read->fetchAll($select)) {
            $segmentsArray = array();
            foreach ($data as $row) {
                $segmentsArray[] = $row['segment_id'];
            }
        }

        return $segmentsArray;
    }
}