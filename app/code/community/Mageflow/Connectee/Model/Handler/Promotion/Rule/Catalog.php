<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Catalog.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Handler_Promotion_Rule_Catalog
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Handler_Promotion_Rule_Catalog
    extends Mageflow_Connect_Model_Handler_Promotion_Rule_Catalog
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function processData(array $data = array())
    {
        try {

            $data = isset($data[0]) ? $data[0] : $data;
            $message = 'success';
            $savedEntity = null;

            foreach($data['related_banners'] as $key => $bannerMfGuid) {
                $bannerId = Mage::getModel('enterprise_banner/banner')
                    ->load($bannerMfGuid, 'mf_guid')->getBannerId();
                if ($bannerId) {
                    $data['related_banners'][$key] = $bannerId;
                } else {
                    unset($data['related_banners'][$key]);
                }

            }

            $savedEntity = $this->processPromotionRuleCatalog($data);

        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $this->log($ex->getMessage());
            $this->log($ex->getTraceAsString());
        }

        return $this->sendProcessingResponse($savedEntity, $message);
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     *
     * @return stdClass
     */
    public function packData(Mage_Core_Model_Abstract $model)
    {
        $c = parent::packData($model);

        $c->related_banners = array();
        $bannerIdCollection = Mage::getModel('enterprise_banner/banner')
            ->getResource()->getRelatedBannersByCatalogRuleId($model->getRuleId());

        foreach ($bannerIdCollection as $bannerId) {
            $c->related_banners[] = Mage::getModel('enterprise_banner/banner')
                ->load($bannerId)->getMfGuid();
        }
        return $c;
    }
}