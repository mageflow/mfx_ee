<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Checkout.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Handler_Promotion_Rule_Checkout
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Handler_Promotion_Rule_Checkout
    extends Mageflow_Connect_Model_Handler_Promotion_Rule_Checkout
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function processData(array $data = array())
    {
        try {

            $data = isset($data[0]) ? $data[0] : $data;
            $message = 'success';
            $savedEntity = null;

            foreach($data['related_banners'] as $key => $bannerMfGuid) {
                $data['related_banners'][$key] = Mage::getModel('enterprise_banner/banner')
                    ->load($bannerMfGuid, 'mf_guid')->getBannerId();
            }

            $savedEntity = $this->processPromotionRuleCheckout($data);

        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $this->log($ex->getMessage());
            $this->log($ex->getTraceAsString());
        }

        return $this->sendProcessingResponse($savedEntity, $message);
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     *
     * @return stdClass
     */
    public function packData(Mage_Core_Model_Abstract $model)
    {
        $c = parent::packData($model);

        $c->related_banners = array();
        $bannerIdCollection = Mage::getModel('enterprise_banner/banner')
            ->getResource()->getRelatedBannersBySalesRuleId($model->getRuleId());

        foreach ($bannerIdCollection as $bannerId) {
            $c->related_banners[] = Mage::getModel('enterprise_banner/banner')
                ->load($bannerId)->getMfGuid();
        }
        return $c;
    }

    /**
     * maps id-s to mf_guid-s
     *
     * @param array $conditions
     *
     * @return array
     */
    protected function processConditions(Array $conditions)
    {
        if ($conditions['type'] == 'enterprise_customersegment/segment_condition_segment') {
            $targetEntity = Mage::getModel('enterprise_customersegment/segment')
                ->load($conditions['value']);
            $conditions['value'] = $targetEntity->getMfGuid();
        }

        if ($conditions['attribute'] == 'attribute_set_id') {
            $targetEntity = Mage::getModel('eav/entity_attribute_set')
                ->load($conditions['value']);
            $conditions['value'] = $targetEntity->getMfGuid();
        }

        if ($conditions['attribute'] == 'category_ids') {
            $categoryIds = explode(', ', $conditions['value']);
            $targetMfGuids = array();
            foreach ($categoryIds as $categoryId) {
                $targetEntity = Mage::getModel('catalog/category')->load($categoryId);
                $targetMfGuids[] = $targetEntity->getMfGuid();
            }
            $conditions['value'] = implode(', ', $targetMfGuids);
        }

        if (isset($conditions['conditions'])) {
            foreach ($conditions['conditions'] as $key => $condition) {
                $conditions['conditions'][$key] = $this->processConditions($condition);
            }

        }

        return $conditions;
    }

    /**
     * create conditions from data array
     *
     * @param array $conditions
     *
     * @return false|Mage_Core_Model_Abstract
     * @throws Exception
     */
    protected function createConditions(array $conditions)
    {
        if (is_null($conditions['attribute'])) {
            /**
             * @var Mage_CatalogRule_Model_Rule_Condition_Combine $conditionEntity
             */
            $conditionEntity = Mage::getModel('catalogrule/rule_condition_combine');
        } else {
            $conditionEntity = Mage::getModel('catalogrule/rule_condition_product');
        }

        if ($conditions['type'] == 'enterprise_customersegment/segment_condition_segment') {
            $collection = Mage::getModel('enterprise_customersegment/segment')
                ->getCollection()->addFieldToFilter('mf_guid', $conditions['value']);
            $targetEntity = $collection->getFirstItem();
            $conditions['value'] = $targetEntity->getSegmentId();
        }

        if ($conditions['attribute'] == 'attribute_set_id') {
            $collection = Mage::getModel('eav/entity_attribute_set')
                ->getCollection()->addFieldToFilter('mf_guid', $conditions['value']);
            $targetEntity = $collection->getFirstItem();
            $conditions['value'] = $targetEntity->getAttributeSetId();
        }

        if ($conditions['attribute'] == 'category_ids') {
            $categoryMfGuids = explode(', ', $conditions['value']);
            $targetIds = array();
            foreach($categoryMfGuids as $categoryMfGuid) {
                $targetEntity =Mage::getModel('catalog/category')->getCollection()->addFieldToFilter(
                    array(
                        array('attribute'=>'mf_guid', 'eq' => $categoryMfGuid)
                    )
                )->getFirstItem();

                if ($targetEntity->getEntityId()) {
                    $targetIds[] = $targetEntity->getEntityId();
                }
            }
            $conditions['value'] = implode(', ', $targetIds);
        }

        if (isset($conditions['conditions'])) {
            foreach ($conditions['conditions'] as $key => $condition) {
                $conditions['conditions'][$key] = $this->createConditions($condition);
            }
        }

        return $conditions;
    }

    /**
     * save item
     *
     * @param Mage_Core_Model_Abstract $model
     * @param                          $data
     *
     * @return Mage_Core_Model_Abstract|object
     * @throws Exception
     */
    public function saveItem($model, $data)
    {
        $model->setData($data);
        $model->setConditionsSerialized(serialize($this->createConditions($data['conditions'])));
        $model->setActionsSerialized(serialize($this->createConditions($data['actions'])));
        $model->save();
        $this->log(sprintf('Saved %s with id=%s', get_class($model), $model->getId()));

        foreach($data['coupons'] as $couponData) {
            $couponData['rule_id'] = $model->getRuleId();
            $couponModel = Mage::getModel('salesrule/coupon');
            $couponModel->setData($couponData);
            $couponModel->save();
        }
        return $model;
    }

}