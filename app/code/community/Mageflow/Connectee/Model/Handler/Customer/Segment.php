<?php
/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Segment.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Handler_Customer_Segment
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Handler
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Handler_Customer_Segment
    extends Mageflow_Connectee_Model_Handler_Abstract
{
    /**
     * update or create cms/page from data array
     *
     * @param array $data
     *
     * @return array
     * @throws Exception
     */
    public function processData(array $data)
    {
        $data = isset($data[0]) ? $data[0] : $data;
        $savedEntity = null;
        $message = null;

        if (isset($data['website_ids'])) {
            $websiteIds = array();
            foreach ($data['website_ids'] as $websiteCode) {
                $websiteIds[] = $this->getWebsiteIdByCode($websiteCode);
            }
            $data['website_ids'] = $websiteIds;
        }

        $model = $this->findModel('enterprise_customersegment/segment', $data['mf_guid']);
        $data['segment_id'] = $model->getSegmentId();

        try {
            $savedEntity = $this->saveItem($model, $data);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $this->log($ex->getMessage());
            $this->log($ex->getTraceAsString());
        }

        return $this->sendProcessingResponse($savedEntity, $message);
    }

    /**
     * packs data for changeset
     *
     * @param Mage_Core_Model_Abstract $model
     *
     * @return stdClass
     */
    public function packData(Mage_Core_Model_Abstract $model)
    {
        $c = $this->packModel($model);
        $c->website_ids = array();

        foreach ($model->getWebsiteIds() as $websiteId) {
            $c->website_ids[] = $this->getWebsiteCodeById($websiteId);
        }

        return $c;
    }}