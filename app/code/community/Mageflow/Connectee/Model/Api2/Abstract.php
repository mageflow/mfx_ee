<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Abstract.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Api2_Abstract
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Api2_Abstract
    extends Mageflow_Connect_Model_Api2_Abstract
{
    /**
     * Dispatches API request
     *
     * @throws Exception
     * @return void
     */
    public function dispatch()
    {
        /**
         * NB! This is a hack. A REST API should be stateless. It can be thought if this "session"
         * below as a container for global variables. There should be (and there isn't) actual session
         * used as long as API requests don't use cookies.
         *
         * @var Mage_Admin_Model_User $userModel
         */
        $userModel = Mage::getModel('admin/user')->load($this->getApiUser()->getUserId());
        Mage::getSingleton('admin/session')->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
        Mage::getSingleton('admin/session')->setUser($userModel);
        return parent::dispatch();
    }
} 