<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * V1.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Model_Api2_Customer_Segment_Rest_Admin_V1
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Model_Api2_Customer_Segment_Rest_Admin_V1
    extends Mageflow_Connectee_Model_Api2_Abstract
{
    /**
     * resource type
     *
     * @var string
     */
    protected $_resourceType = 'enterprise_customersegment/segment';

    /**
     * CREATE new CustomerSegment on POST request
     * it's also used for UPDATE on PUT request
     *
     * @param array $filteredData
     *
     * @return array|string|void
     * @throws Exception
     * @throws Mage_Api2_Exception
     */
    public function _create(array $filteredData)
    {
        $model = Mage::getModel('enterprise_customersegment/segment');

        $response = $this->getDataProcessor($model)->processData($filteredData);

        if ($response['status'] == 'success') {
            $this->_successMessage(
                sprintf(
                    'Successfully created or updated %s',
                    $this->getResourceType()
                ), 200, $response
            );
        } else {
            $this->_critical('An error occurred while updating entity', 500);
        }

        return;
    }

    /**
     * GET request to retrieve a single customer segment
     *
     * @return array|mixed
     */
    public function _retrieve()
    {
        $this->log(print_r($this->getRequest()->getParams(), true));
        $storeId = $this->getRequest()->getParam('store', -1);
        $this->log($storeId);

        $mfGuid = $this->getRequest()->getParam('mf_guid');

        $out = array();
            $collection = $this->getWorkingModel()->getCollection();
            $collection->addFieldToFilter('mf_guid', $mfGuid);
            $entity = $collection->getFirstItem();

        if ($entity instanceof Enterprise_Banner_Model_Banner) {
            $out[] = $this->packModel($entity);
        }

        return $out;
    }

    /**
     * DELETE to delete a collection of customer segments
     *
     * @param array $filteredData
     */
    public function _multiDelete(array $filteredData)
    {
        $this->log($filteredData);

        $segmentEntity = Mage::getModel('enterprise_customersegment/segment')
            ->load($filteredData['mf_guid'], 'mf_guid');

        $originalData = $segmentEntity->getData();
        $rollbackFeedback = array();
        // send overwritten data to mageflow
        if ($originalData) {
            $rollbackFeedback = $this->sendRollback(
                str_replace('_', ':', $this->_resourceType),
                $filteredData,
                $originalData
            );
        } else {
            $this->sendJsonResponse(
                array('notice' => 'target not found or empty, mf_guid='
                    . $filteredData['mf_guid'])
            );
        }
        try {
            $segmentEntity->delete();
            $this->sendJsonResponse(
                array_merge(
                    array('message' =>
                              'target deleted, mf_guid=' . $filteredData['mf_guid']),
                    $rollbackFeedback
                )
            );
        } catch (Exception $e) {
            $this->sendJsonResponse(
                array_merge(
                    array('delete error' => $e->getMessage()),
                    $rollbackFeedback
                )
            );
        }
    }


}
