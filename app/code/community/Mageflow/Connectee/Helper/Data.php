<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Data.php
 *
 * PHP version 5
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connectee_Helper_Data
 *
 * @category   MFXEE
 * @package    Mageflow_Connectee
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Helper_Data extends Mageflow_Connect_Helper_Data
{
    /**
     * create changesetitem object of type from content
     * type must be with ":", like "cms:block"
     * content must be array from getData()
     *
     * MFX EE version extends MFX CE to:
     * 1) process special case of cloned cms nodes
     *
     * @param stdClass $type
     * @param null     $model
     * @param boolean  $forceValidation
     *
     * @internal param $content
     *
     * @return Mageflow_Connect_Model_Changeset_Item
     */
    public function createChangesetFromItem($type, $model = null, $forceValidation = false)
    {
        if ($type->short == 'enterprise_cms/hierarchy_node') {
            $mfGuid = $model->getMfGuid();
            $collection = Mage::getModel('enterprise_cms/hierarchy_node')
                ->getCollection()
                ->addFieldToFilter('mf_guid', $mfGuid);

            if ($collection->getSize() > 1) {
                $model->setMfGuid($this->randomHash(32));
                $model->setData('mf_guid_needs_update', true);
            }
        }

        return parent::createChangesetFromItem($type, $model, $forceValidation);
    }

    /**
     * get module version from config
     */
    public function getModuleVersion()
    {
        return Mage::getConfig()->getModuleConfig('Mageflow_Connectee')->version;
    }

    /**
     * get module version from config
     */
    public function getModuleName()
    {
        return Mage::getConfig()->getModuleConfig('Mageflow_Connectee')->name;
    }
} 