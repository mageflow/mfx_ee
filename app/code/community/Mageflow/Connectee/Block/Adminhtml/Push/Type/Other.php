<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 *  If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 *  obtaining an appropriate licence.
 */
/**
 * Grid.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 *  @author     MageFlow OÜ, Estonia <info@mageflow.com>
 *  @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Block_Adminhtml_Migrate_Grid
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 *  @author     MageFlow OÜ, Estonia <info@mageflow.com>
 *  @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_Block_Adminhtml_Push_Type_Other extends Mageflow_Connectee_Block_Adminhtml_Push_Grid {

    /**
     * type
     * 
     * @var type 
     */
    protected $_itemType = 'other';

    /**
     * prepare collection
     * OTHER is a bit complicated type and gets special treatment
     *
     * @return Mageflow_Connect_Block_Adminhtml_Push_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getModel('mageflow_connect/changeset_item')->getCollection();
        $collection->addFieldToFilter('type', array('nin' => $this->getTypeListing()));
        $collection->addFieldToFilter('is_current', array('eq' => true));
        $collection->printLogQuery(false, true);

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

}
