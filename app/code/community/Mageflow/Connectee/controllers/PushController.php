<?php

/**
 * PushController.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Controller
 *  @author     MageFlow OÜ, Estonia <info@mageflow.com>
 *  @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

require_once 'Mageflow/Connect/controllers/PushController.php';
/**
 * Mageflow_Connect_PushController
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Controller
 *  @author     MageFlow OÜ, Estonia <info@mageflow.com>
 *  @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connectee_PushController extends Mageflow_Connect_PushController {

 
    /**
     * this function actually makes the view
     * shall be updated when grid loeading is updated to ajax
     * 
     * @param type $gridBlockName
     */
    protected function composeView($gridBlockName, $type = 'cmspages') {
        $this->loadLayout();

        $isAjax = $this->getRequest()->getParam('isAjax', false);

        if ($isAjax) {
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock($gridBlockName, 'mageflow_connect.pushgrid')->toHtml()
            );
        } else {
            $tabBlock = $this->getLayout()->createBlock(
                    'mageflow_connectee/adminhtml_push_tabs', 'mageflow_connect.tabs'
            );
            $this->log($type);
            $tabBlock->setActiveTab($type);

            $gridBlock = $this->getLayout()
                    ->createBlock($gridBlockName, 'mageflow_connect.pushgrid');

            $this->_addLeft($tabBlock);
            $this->_addContent($gridBlock);
            $this->renderLayout();
        }
    }

    /**
     * returns grid
     */
    public function cmsnodesAction() {
        return $this->composeView('mageflow_connectee/adminhtml_push_type_node', 'cmsnodes');
    }    
    
    /**
     * returns grid
     */
    public function otherAction() {
        return $this->composeView('mageflow_connectee/adminhtml_push_type_other', 'other');
    }        
}
