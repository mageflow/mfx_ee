![MageFlow-Enterprise-Connector-250x250.png](https://bitbucket.org/repo/Gpzb9n/images/300759704-MageFlow-Enterprise-Connector-250x250.png)

# README #

MageFlow Connect EE extension (MFX_EE) extends MageFlow Connect extension ([MFX](https://bitbucket.org/mageflow/mfx) ).
It adds support for Magento EE specific features like CMS Banners, CMS Page versioning and hierarchy and more.

Support for Magento Enterprise 1.14 has been throughly tested. Support for older Magento EE version is not guaranteed and should be considered "as is".

### What is this repository for? ###

* This repository contains MageFlow Connect EE extension for Magento 1.*
* Current major version is 1.0beta

## Beta ##
**This is a beta release. Do not use in production environments! Please contact enterprise@mageflow.com for more information.**

## Releases ##

[Release Notes](https://bitbucket.org/mageflow/mfx_ee/wiki/ReleaseNotes) can be found from [MFX EE Development Wiki](https://bitbucket.org/mageflow/mfx_ee/wiki)

#### Installing extension with modman ####

Always **install MFX before** (at least version MFX 1.4.6) installing MFX_EE , follow the [instructions here](https://bitbucket.org/mageflow/mfx).
Please note, that as the MFX_EE depends on and extends MFX. It can not be used without MFX.

To install together with MFX, run in your Magento folder

```
#!bash
modman init
modman clone git@bitbucket.org:mageflow/mfx.git
modman clone git@bitbucket.org:mageflow/mfx_ee.git
```

If MFX is already installed, then run

```
#!bash
modman init
modman clone git@bitbucket.org:mageflow/mfx_ee.git
```

To update, run 

```
#!bash
modman update mfx_ee
```

### Symlinks and modman ###

In order for a modman-installed module to work properly with Magento support for symlinks must be enabled. Support for symlinks in Magento templates can be enabled under System->Configuration->Advanced->Developer->Template settings.

### Magento update scripts ###

An update script currently modifies the following tables in your Magento's database:
* enterprise_banner
* enterprise_cms_hierarchy_node
* enterprise_customersegment_segment
by adding fields mf_guid, created_at and updated_at

## Uninstalling ##

MFX_EE can be removed with modman. Removing it together with MFX:
```
#!bash
modman remove mfx_ee
modman remove mfx
```
Before uninstalling, please:
1. Make sure you do have a backup of your database
2. Delete all records from your Magento database table eav_attribute where attribute_code='mf_guid'

## Who do I talk to? ##

* Contact MageFlow at info@mageflow.com

# License info #

```
#!php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 * 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
```